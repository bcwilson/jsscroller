(function() {

var ns = {};


ns.scrollerData = [];

// sample data, set on page load or from api req
ns.scrollerData.push({
		"company": "Space Jam", 
		"name": "Astro",
		"description": "it's the astro desc!",
		"image": "img/SpaceJamAstro.jpg",
		"link": "#SpaceJamAstro"
});
ns.scrollerData.push({
		"company": "Space Jam", 
		"name": "Andromeda",
		"description": "it's the andomeda desc!",
		"image": "img/SpaceJameAndromeda.jpg",
		"link": "#SpaceJameAndromeda"
});
ns.scrollerData.push({
		"company": "Space Jam", 
		"name": "Eclipse",
		"description": "it's the eclipse desc!",
		"image": "img/SpaceJamEclipse.jpg",
		"link": "#SpaceJamEclipse"
});
ns.scrollerData.push({
		"company": "Space Jam", 
		"name": "Galactica",
		"description": "it's the galactica desc!",
		"image": "img/SpaceJameGalactica.jpg",
		"link": "#SpaceJamGalactica"
});
ns.scrollerData.push({
		"company": "Space Jam", 
		"name": "Omega",
		"description": "it's the omega desc!",
		"image": "img/SpaceJamOmega.jpg",
		"link": "#SpaceJamOmega"
});
ns.scrollerData.push({
	"company": "Space Jam", 
	"name": "Pluto",
	"description": "it's the pluto desc!",
	"image": "img/SpaceJamPluto.jpg",
	"link": "#SpaceJamPluto"
});
ns.scrollerData.push({
	"company": "Space Jam", 
	"name": "Starship",
	"description": "it's the starship desc!",
	"image": "img/SpaceJamStarship1.jpg",
	"link": "#SpaceJamStarship"
});
ns.scrollerData.push({
	"company": "Space Jam", 
	"name": "Venus",
	"description": "it's the venus desc!",
	"image": "img/SpaceJamVenus.jpg",
	"link": "#SpaceJamVenus"
});


//
//  scroller item data model with observable props
//
ns.ScrollerItem = function( data ) {
	var self = this;

	// set data if not passed
	data = data || {};

	//
	// default values for new model
	//
	self.defaults = {
			'id': "",
			'company': "",
			'description': "",
			'image': "", // TODO: use a generic image?
			'link': "#",
			'name': "",
	}
	self.id = ko.observable( self.defaults["id"] );
	self.company = ko.observable( self.defaults["company"] );
	self.description = ko.observable( self.defaults["description"] );
	self.image = ko.observable( self.defaults["image"] );
	self.link = ko.observable( self.defaults["link"] );
	self.name = ko.observable( self.defaults["name"] );
	
	self.hasHover = ko.observable( false );

	//
	// will reset/create from defaults, can optionally pass data to use
	//
	self.resetValues = function( data ) {
		// set data if needed
		data = data || {};
		// iterate default props and change observables
		if ( typeof self.defaults !== "undefined" ) {
			for ( var k in self.defaults ) {
				if ( self.defaults.hasOwnProperty(k) ) {
			 		// set from data, else defaults
					var val = ( typeof data[k] !== "undefined" ? data[k] : self.defaults[k] );
			 		self[k]( val );
				}
			}
		}
	};
	self.resetValues( data );
};

//
// scroller knockout.js view model
//
ns.Scroller = function() {
	var self = this;

	//
	// observable array for storing data items
	//
	self.items = ko.observableArray();

	//
	// the selected item
	//
	self.selectedItem = new ns.ScrollerItem();
	
	// static scrollItem width (200) + left+right margin (5+5)
	// static left position of scrollList (-205)
	// TODO: should be dynamic
	self.scrollItemWidth = 210;
	self.scrollListPositionLeft = -205;

	//
	// add a new data item, returns newly created model
	//
	self.add = function( data ) {
		var newModel = new ns.ScrollerItem( data );
		self.items.push( newModel );
		return newModel;
	};

	//
	// iterate data as array and add
	//
	self.addArray = function( data ) {
		if ( data && data.length ) {
			for ( var i = 0; i < data.length; i++ ) {
				self.add( data[i] );
			}
		}
	};

	//
	// hovers for main container element
	//
	self.containHoverStart = function( data, e ) {
		$( ".scrollButton", $( e.currentTarget ) ).fadeIn( 200 )
	};
	self.containHoverStop = function( data, e ) {
		$( ".scrollButton", $( e.currentTarget ) ).fadeOut( 100 )
	};

	//
	// change data for selectedItem from a ScrollerItem model
	//
	self.changeSelectedItem = function( ScrollerItem ) {
		self.selectedItem.resetValues({
			'id': ScrollerItem.id(),
			'company': ScrollerItem.company(),
			'description': ScrollerItem.description(),
			'image': ScrollerItem.image(),
			'link': ScrollerItem.link(),
			'name': ScrollerItem.name()
		});
	};

	//
	// move data items around for infinite scrolling
	//
	self.pushNext = function() {
		// move first item to last
		var arr = self.items.shift();
		self.items.push( arr );
	};
	self.pushPrev = function() {
		// move last item to first
		var arr = self.items.pop();
		self.items.unshift( arr );
	};

	//
	// remove all data items and optionally add data as array
	//
	self.resetValues = function( data ) {
		self.items.removeAll();
		self.addArray( data );
	};

	//
	// scroll to next item from a click
	//
	self.scrollClickNext = function( data, e ) {
		var $scrollContain = $( e.currentTarget.parentElement ),
		    $scrollList = $( ".scrollList", $scrollContain );

		// new position to animate to
		var positionX = self.scrollListPositionLeft - self.scrollItemWidth;
		// animate!
		$scrollList.animate({ "left" : positionX}, {
				'queue': true, 
				'duration': 500, 
				'done': function() {
					// move first item to end of list for infinite scrolling
					self.pushNext();
					// enforce animation end position
					$scrollList.css({ "left": self.scrollListPositionLeft });
    		}
    });
	};

	//
	// scroll to prev item from a click
	//
	self.scrollClickPrev = function( data, e ) {
		var $scrollContain = $( e.currentTarget.parentElement ),
		    $scrollList = $( ".scrollList", $scrollContain );

		// new position to animate to
		var positionX = self.scrollListPositionLeft + self.scrollItemWidth;
		// animate!
		$scrollList.animate({ 'left' : positionX }, {
				'queue': true, 
				'duration': 500, 
				'always': function() {
					// move last item to beginning of list for infinite scrolling
					self.pushPrev();
					// enforce animation end position
					$scrollList.css({ 'left': self.scrollListPositionLeft });
    		}
    });
	};

	//
	// scroll delay for hover
	//
	self.scrollHoverDelay = 2000;
	self.scrollHover = false;

	//
	// scroll to next item from a hover
	//
	self.scrollHoverNextStart = function( data, e ) {
		self.scrollHover = true;

		var $scrollContain = $( e.currentTarget.parentElement ),
        $scrollList = $( ".scrollList", $scrollContain );

		// static scrollItem width (200) + left+right margin (5+5)
		// static left position of scrollList (-205)
		// TODO: should be dynamic
		var scrollDelay = self.scrollHoverDelay,
		    // new position to animate to
				positionX = self.scrollListPositionLeft - self.scrollItemWidth,
				scrollListLeft = Math.abs( Math.round( parseInt( $scrollList.css( "left" ) ) ) ),
		    remainingToNext = ( ( Math.abs( positionX ) - scrollListLeft ) / self.scrollItemWidth );

		if ( 1 > remainingToNext ) {
			scrollDelay = Math.round( scrollDelay * remainingToNext );
		}

		// animate!
		$scrollList.animate({ 'left' : positionX }, {
				'queue': false, 
				'duration': scrollDelay, 
				'easing': "linear",
				'complete': function() {
					// move first item to end of list for infinite scrolling
					self.pushNext();
					// enforce animation end position
					$scrollList.css({ 'left': self.scrollListPositionLeft });
					// loop?
					if ( self.scrollHover ) {
						self.scrollHoverNextStart( data, e );
					}
				}
		});
	};

	//
	// scroll to prev item from a hover
	//
	self.scrollHoverPrevStart = function( data, e ) {
		self.scrollHover = true;

		var $scrollContain = $( e.currentTarget.parentElement ),
        $scrollList = $( ".scrollList", $scrollContain );

		// static scrollItem width (200) + left+right margin (5+5)
		// static left position of scrollList (-205)
		// TODO: should be dynamic
		var scrollDelay = self.scrollHoverDelay,
		    // new position to animate to
		    positionX = self.scrollListPositionLeft + self.scrollItemWidth,
		    scrollListLeft = Math.abs( Math.round( parseInt( $scrollList.css( "left" ) ) ) ),
		    remainingToNext = ( scrollListLeft / Math.abs( self.scrollListPositionLeft ) );

		if ( 1 > remainingToNext ) {
			scrollDelay = Math.round( scrollDelay * remainingToNext );
		}

		// animate!
		$scrollList.animate({ 'left' : positionX }, {
				'queue': false, 
				'duration': scrollDelay, 
				'easing': "linear",
				'complete': function() {
					// move last item to beginning of list for infinite scrolling
					self.pushPrev();
					// enforce animation end position
					$scrollList.css({ 'left': self.scrollListPositionLeft });
					// loop?
					if ( self.scrollHover ) {
						self.scrollHoverPrevStart( data, e );
					}
				}
		});
	};

	//
	// stop scrolling from hover
	//
	self.scrollHoverStop = function( data, e ) {
		self.scrollHover = false;
		var $scrollContain = $( e.currentTarget.parentElement ),
        $scrollList = $( ".scrollList", $scrollContain );
		$scrollList.stop();
	};

	//
	// tooltip timeout props
	//
	self.tooltipHoverDelay = 750;
	self.tooltipTimeout = false;

	//
	// scrollItem hover start
	//
	self.tooltipHoverStart = function( data, e ) {
		// clear existing timeout
		if ( self.tooltipTimeout ) {
			clearTimeout( self.tooltipTimeout );
			self.tooltipTimeout = false;
		}
		// get elements needed
		var $scrollItem = $( e.currentTarget.parentElement ),
		    $scrollItemOverlay = $( ".overlay", $scrollItem ),
		    $scrollContain = $scrollItem.parents( ".scrollContain" ),
		    $scrollList = $( ".scrollList", $scrollContain ),
		    $tooltipItem = $( ".tooltipItem", $scrollContain );
		// show scrollItem overlay, slide company and name out
		$scrollItemOverlay.show( 0 );
		$( ".company", $scrollItem ).fadeOut( 150 );
		$( ".name", $scrollItem ).fadeOut( 150 );
		// determine where we want the tooltip to appear
		var position = $scrollItem.position(),
		    positionY = ( position.top + Math.round( $scrollItem.height() * .35 ) ),
		    positionX = position.left;
		// account for incomplete scroll animations
		positionX += ( Math.abs( self.scrollListPositionLeft ) - Math.abs( parseInt( $scrollList.css( "left" ) ) ) );
		// show on left or right side of scrollItem
		if ( positionX > ( $scrollContain.width() / 2 ) ) {
			// show on left
			positionX = ( positionX - ( $scrollItem.width() + $tooltipItem.width() ) );
			// move it slightly over the scrollItem
			positionX += Math.round( $scrollItem.width() * .05 );
		} else {
			// show on right
			// move it slightly over the scrollItem
			positionX -= Math.round( $scrollItem.width() * .1 );
		}
		// set new timeout
		self.tooltipTimeout = setTimeout( function() {
			self.tooltipItemShow( data, $tooltipItem, positionX, positionY );
		}, self.tooltipHoverDelay );
	};

	//
	// scrollItem hover stop
	//
	self.tooltipHoverStop = function( data, e ) {
		// clear existing timeout
		if ( self.tooltipTimeout ) {
			clearTimeout( self.tooltipTimeout );
			self.tooltipTimeout = false;
		}
		var $scrollItem = $( e.currentTarget.parentElement ),
        $scrollItemOverlay = $( ".overlay", $scrollItem ),
        $scrollContain = $scrollItem.parents ( ".scrollContain" ),
        $tooltipItem = $( ".tooltipItem", $scrollContain );
		// hide scrollItem overlay
		$scrollItemOverlay.hide( 0 );
		$( ".company", $scrollItem ).show( 0 );
		$( ".name", $scrollItem ).show( 0 );
		// set new timeout
		self.tooltipTimeout = setTimeout( function() {
			self.tooltipItemHide( $tooltipItem );
		}, self.tooltipHoverDelay );
	}

	//
	// tooltipItem hover start
	//
	self.tooltipItemHoverStart = function( data, e ) {
		// clear existing timeout
		if ( self.tooltipTimeout ) {
			clearTimeout( self.tooltipTimeout );
			self.tooltipTimeout = false;
		}
	}

	//
	// tooltipItem hover stop
	//
	self.tooltipItemHoverStop = function( data, e ) {
		// set new timeout
		self.tooltipTimeout = setTimeout( function() {
			self.tooltipItemHide( $( e.currentTarget ) );
		}, self.tooltipHoverDelay );
	}

	//
	// hide the tooltip element
	//
	self.tooltipItemHide = function( $tooltipItem ) {
		$tooltipItem.hide( 0 );
	};

	//
	// show the tooltip element
	//
	self.tooltipItemShow= function( data, $tooltipItem, positionX, positionY ) {
		// change the selectedItem data
		self.changeSelectedItem( data );
		// position the element and show it
		$tooltipItem.css( "top", positionY );
		$tooltipItem.css( "left", positionX );
		$tooltipItem.fadeIn( 100 );
	};

};


$( document ).ready(function() {
	// create a new scrollers and add data
	var clickScroller = new ns.Scroller();
	clickScroller.addArray( ns.scrollerData );
	var hoverScroller = new ns.Scroller();
	hoverScroller.addArray( ns.scrollerData );
	// bind to document
	ko.applyBindings({
		"clickScroller": clickScroller,
		"hoverScroller": hoverScroller
	});
});

}());
